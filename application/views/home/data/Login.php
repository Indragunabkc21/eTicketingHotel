<div class="inside-banner">
  <div class="container"> 
   
    <h2>Login</h2>
</div>
</div>
<!-- banner -->


<div class="container">
<div class="spacer">
<div class="row">
  <div class="col-lg-4  col-lg-offset-4"><center>
  
		<!-- Default form login -->
		<form method="POST" class="form-horizontal" action="<?= site_url('Home/login') ?>">
		    <p class="h4 text-center mb-4">Login</p>

		    <!-- Default input email -->
		    <label for="defaultFormLoginEmailEx" class="grey-text">Username</label>
		    <input type="text" id="defaultFormLoginEmailEx" class="form-control" name="user">

		    <br>

		    <!-- Default input password -->
		    <label for="defaultFormLoginPasswordEx" class="grey-text">Password</label>
		    <input type="password" id="defaultFormLoginPasswordEx" class="form-control" name="pass">

		    <div class="text-center mt-4">
		        <button class="btn btn-primary" type="submit">Masuk</button>
		    </div>
		</form>
		<!-- Default form login --> 
		</center>
 </div>
 
</div>
</div>
</div>