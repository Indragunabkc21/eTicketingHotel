<div class="inside-banner">
  <div class="container"> 
    
    <h2>Kamar</h2>
</div>
</div>
<!-- banner -->


<div class="container">
<div class="properties-listing spacer">


<div class="col-lg-11 col-sm-10">

<div class="row">


     <?php
      foreach ($kamar->result_array() as $value) { ?>
     <!-- properties -->
      <div class="col-lg-4 col-sm-6">

        
        <div class="properties">
          <div class="image-holder"><img src="<?php echo base_url();?>images/kamar_gambar/<?php echo $value['nama_kamar_gambar'];?>" class="img-responsive" alt="properties"/>
            <?php
            if ($value['status_kamar']=="0") { ?>
            <div class="status sold">Avaliable</div>

            <?php
            }
            else { ?>

            <div class="status new">No Avaliable</div>
            <?php
            }
            ?>
          </div>
          <h4><a href="<?php echo base_url();?>home/kamar_detail/<?php echo $value['id_kamar'];?>"><?php echo $value['nomer_kamar'];?></a></h4>
          <p class="price">Harga: <?php echo rupiah($value['harga_kamar']);?></p>
		  <div class="listing-detail"><?php echo $value['nama_kelas_kamar'];?>   </div>
          <a class="btn btn-primary" href="<?php echo base_url();?>home/kamar_detail/<?php echo $value['id_kamar'];?>">selengkapnya</a>
        </div>


      </div>
      <!-- properties -->

      <?php
      }
      ?>

    
      <div class="center">

</div>

</div>
</div>
</div>
</div>
</div>