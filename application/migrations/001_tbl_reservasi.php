<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Tbl_reservasi extends CI_Migration {

        public $table = 'tbl_reservasi';
        public function up()
        {
                $id = 'id_'.$this->table;
                $this->dbforge->add_field(array(
                        'id_reservasi' => array(
                                'type' => 'int',
                                'null' => FALSE,
                                'auto_increment' => TRUE
                        ),
                        'nama_reservasi' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '25',
                                'null' => FALSE
                        ),
                        'telp_resevasi' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '12',
                                'null' => FALSE
                        ),
                        'alamat_reservasi' => array(
                                'type' => 'TEXT',
                                'null' => FALSE
                        ),
                        'tgl_reservasi_masuk' => array(
                                'type' => 'DATETIME',
                                'null' => FALSE
                        ),
                        'tgl_reservasi_KELUAR' => array(
                                'type' => 'DATETIME',
                                'null' => FALSE
                        ),
                        'kamar_id' => array(
                                'type' => 'INT',
                                'constraint' => '11',
                                'null' => TRUE
                        ),
                        'status_reservasi' => array(
                                'type' => 'INT',
		                        'constraint' => '2',
                                'null' => TRUE
                        ),
                ));
                $this->dbforge->add_key($id, TRUE);
                $this->dbforge->create_table($this->table);
        }

        public function down()
        {
                $this->dbforge->drop_table($this->table);
        }
}

?>